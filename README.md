Try ProcessMaker using Docker Compose
=====================================

Easily install and try ProcessMaker using Docker Compose.

Requirements
------------

You have to install the following tools:

- [Composer](https://getcomposer.org/download/)
- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

Installing
----------

1. Install the project:

    ```
    $ composer create-project jawira/process-maker
    $ cd process-maker
    ```

2. Start `setup` process with Phing. Please be patient.

    ```
    $ vendor/bin/phing setup
    ```
    
3. Once finished, your browser should open at [http://localhost:8080](). Then 
you will see ProcessMaker's installer.

4. Follow [ProcessMaker install instructions](docs/step-1.md).

Controlling Docker Compose
--------------------------

`setup` target should only be done once, after you can start and stop Docker 
containers (web server and database) using Phing:

- To start Docker Compose:

    ```
    $ vendor/bin/phing dc:up
    ```
    
- To stop Docker Compose:

    ```
    $ vendor/bin/phing dc:stop
    ```

- To remove containers, networks and volumes: 

    ```
    $ vendor/bin/phing dc:down
    ```

- Open [http://localhost:8080]() in your browser:

    ```
    $ vendor/bin/phing open:localhost
    ```

Links
-----

- Install ProcessMaker in a Linux Generic Installation

    <https://wiki.processmaker.com/3.0/ProcessMaker_Generic_Installation>

- Getting Started Guide

    <https://wiki.processmaker.com/3.0/getting_started>
    
