Changelog
=========

All notable changes to this project will be documented in this file.

<!--
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
-->

Unreleased
----------

## [v1.0.1] - 2018-11-15

### Changed

- Renaming project to `jawira/process-maker`

## [v1.0.0] - 2018-11-15

### Changed

- Process maker is updated to v3.3.0
- Added [tutorial](docs/step-1.md) with install instructions

## [v0.2.0] - 2018-11-07

### Changed

- ProcessMaker is downloaded by `Phing` anymore, but included in `Dockerfile`
- Improving [README.md]() with install instructions and links

## [v0.1.0] - 2018-09-23

### Changed

- Improving [README.md]()

## [v0.0.0] - 2018-07-22

### Added

- First working version, ProcessMaker install successfully but it's not 
production ready.


[v1.0.0]: https://gitlab.com/jawira/process-maker/compare/v0.2.0...v1.0.0
[v0.2.0]: https://gitlab.com/jawira/process-maker/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/jawira/process-maker/compare/v0.0.0...v0.1.0

[v1.0.1]: https://gitlab.com/jawira/process-maker/compare/v1.0.0...v1.0.1
