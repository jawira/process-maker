FROM php:5.6-apache

WORKDIR /var/www/processmaker/

# Fix debconf warnings upon build
# https://github.com/moby/moby/issues/4032#issuecomment-192327844
ARG DEBIAN_FRONTEND=noninteractive

# Apache config
# https://github.com/docker-library/php/issues/75#issuecomment-82075678
COPY ./resources/docker/pmos.conf /etc/apache2/sites-available/

# Custom PHP configuration
COPY resources/docker/99-php-custom.ini /usr/local/etc/php/conf.d/

RUN echo "##### MailHog #####" \
    && mkdir -p /opt/mailhog \
    && curl -s -L -o /opt/mailhog/mhsendmail \
        https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64 \        
    && chmod 755 /opt/mailhog/mhsendmail \
    && echo "##### ProcessMaker #####" \
    && curl -s -L -o ./processmaker.tar.gz \
        https://sourceforge.net/projects/processmaker/files/ProcessMaker/3.3.0/processmaker-3.3.0-community.tar.gz/download \
    && tar xvzf ./processmaker.tar.gz -C /var/www/ \
    && chown -R www-data:www-data /var/www/processmaker \
    && chmod -R 755 /var/www/processmaker \
    && echo "##### Apache configuration #####" \
    && a2enmod vhost_alias deflate rewrite expires \
    && a2dissite *.conf && a2ensite pmos.conf \
    && echo "##### apt-get #####" \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libldap2-dev \
        libmcrypt-dev \
        libpng-dev \
        libxml2-dev \
    && echo "##### PHP extensions #####" \
    && docker-php-source extract \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install \
        gd \
        ldap \
        mcrypt \
        mysql \
        pdo \
        pdo_mysql \
        soap \
    && echo "##### Cleaning #####" \
    && docker-php-source delete \
    && rm ./processmaker.tar.gz \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*
