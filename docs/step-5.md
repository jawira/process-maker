Step 5 - Workspace Configuration
================================

Configure your workspace.

![Step 5](../resources/pictures/step-5.png)

1. Workspace Name: `workspace` (or change if you want).
2. Admin Username: `admin` (or change if you want).
3. Admin Password: anything you want, but remember it for later.
4. Confirm Admin Password: same as preceding step.

5. Check `Change database name` checkbox.
6. Workflow Database Name: `wf_workflow`
7. Check `Delete database if exists` checkbox.
8. Check `Th MySQL user from the...` checkbox.
9. Click on `Check Workspace Configuration` button.
9. Click on `Finish` button.

**[← Step 4](./step-4.md) | [Step 6 →](./step-6.md)**
