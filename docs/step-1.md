Step 1 - Pre-installation check
===============================

All requirements should be satisfied. 

![Step 1](../resources/pictures/step-1.png)

Click on `next` button.

**[Step 2 →](./step-2.md)**
