Step 4 - Database Configuration
===============================

Fill Database details.

![Step 4](../resources/pictures/step-4.png)

1. Database Engine: `MySQL`.
2. Host Name: `pm_mysql_1`.
3. Port: `3306`
4. Username: `root`
5. Password: `groot`
6. Click on `Test Connection` button.
7. Click on `next` button.

**[← Step 3](./step-3.md) | [Step 5 →](./step-5.md)**
